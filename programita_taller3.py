########################################################################
# Taller Flash N° 3
# timestamp y temperatura
# Autor: Gabriel Lichtenstein
# Fecha: 07/05/2016
########################################################################
# usage: python3 programita_taller3.py entrada.csv salida.csv 20       #
########################################################################
#
#
#
########################################################################
# importar libreria que contiene a sys.argv:
import sys
import datetime

# inicilizar variables globales:
csv_infile = (sys.argv[1]) # entrada.csv
csv_outfile = (sys.argv[2]) # salida.csv
ventana = int((sys.argv[3])) # ventana de mediciones (para promediar)

promedio = "NA" # el promedio de c/ ventana de temperaturas
########################################################################
#
#
#
#
########################################################################
# abrir y leer
########################################################################
def abrir_csv(csv_infile):
	mediciones = [] # mediciones de cada sensor en todos los timestamps	
	with open(csv_infile, 'r') as f:
		for line in f:
			line = line.rstrip('\n')
			line = line.split(',')
			mediciones.append(line)		
		return mediciones
########################################################################
#
# iniciar una lista conteniendo las lineas del csv_infile como listas:
csv_rows = (abrir_csv(csv_infile))
#
#~ print(len(csv_rows[0])-0) # debug print
########################################################################
def extraer_columna(sensor): 
	#arma una lista con una columna (sensor) del csv.
	columna = []
	for element in csv_rows: 
		columna.append(element[sensor])
	return (columna)
########################################################################
#
#
#
#
########################################################################
# Promediador
########################################################################
def promediador():
	####################################################################
	# Itera cada sensor del csv, promedia las ventanas deslizantes y
	# calcula el delta de tiempo de cada ventana:
	####################################################################
	matrix=[] # matriz de promedios de c/ ventana de c/ sensor.
	
	sensor = 1 # columna 1, la 0 tiene los timestamps
	while sensor < (len(csv_rows[0])): 
		
		out = [] # lista con la lista de promedios de cada ventana.
		seconds_delta = [] #diferencia entre tiempo final e inicial
		
		# agrega items a la lista final:
		matrix.append(out)
		
		################################################################
		# suma los elementos de c/ ventana y promedia la misma:
		################################################################
		i=0
		while ((i < len(csv_rows)) and (i+ventana <= len(csv_rows))):
		
			suma = 0.0 # sumatoria de la ventana de mediciones
			contador = 0 # cuenta cuando aparece un NA
			ventana_de_mediciones = (extraer_columna(sensor)[i:i+ventana])
			
			for element in (ventana_de_mediciones):	
				if (element != "NA"):
					suma = suma + float(element)
				elif (element == "NA"):
					contador += 1 
					suma = 0.0 # asumo que NA es un valor cero
										
			# devolver promedio con centesimo de precisión:
			promedio = '{:04.2f}'.format((suma / len(ventana_de_mediciones)))
			
			# Controlador de valores NA en la lista final (out):			
			if contador == 0.0:
				out.append(promedio)
				#~ print("&") #debug print
				#~ print(out) #debug print
			else:
				out.append("NA")
				#~ print("@") #debug print
				#~ print(out) #debug print
				
			# tiempo inicial y final de la ventana medida
			t1 = datetime.datetime.strptime((csv_rows[i][0]), '%Y-%m-%dT%H:%M:%S')
			t2 = datetime.datetime.strptime((csv_rows[i+(ventana-1)][0]), '%Y-%m-%dT%H:%M:%S')
			
			#~ print("##") #debug print
			#~ print(out) #debug print

			# acumula el delta de tiempo de cada ventana en una lista:
			seconds_delta.append(str((t2 - t1).total_seconds()))
			i+=1
		################################################################
		sensor+=1
	####################################################################
	# agrega una lista (con el delta de tiempo en segundos) a la lista
	# de promedios, para luego usarlos en el outfile.
	####################################################################
	matrix = [seconds_delta] + matrix
	return(matrix)
########################################################################
#
#
#
#
########################################################################	
def transponedor(matrix):	
	# Transponer la matrix:
	# rota 90 grados una matriz (lista de listas).
	cacahuate = []
	for i in range(0,len(matrix[0])): # listas tienen q tener mismo len
		cacahuate.append([",".join(element[i] for element in matrix)])
	return (cacahuate)	
########################################################################
#
#
#
#
########################################################################
# ejecutar programa y escribir outfile
########################################################################		
with open(csv_outfile, 'w') as f:
	matrix = transponedor(promediador()) # matriz final
	for element in matrix:
		f.write(str(element).strip('[\'\']'))
		f.write('\n')
########################################################################
